using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogSystem : MonoBehaviour
{
    public GameObject window; // caja donde aparecera el dialogo del NPC
    public GameObject indicator; // gameobject referente al indicador de que el NPC puede hablar
    public List<string> dialogues; // lista que guarda las frases del NPC
    private int index; // indice para el orden de los dialogos
    private int charIndex; // indice para el orden de los caracteres de una frase
    private bool started; //indica si ha comenzado el dialogo a mostrarse
    public TMP_Text dialogueText; // componente del texto del dialogo

    public float writingSpeed; // tiempo de espera del texto en mostrarse
    private bool waitForNextDialog; // esperamos a que el player indique que siga el mensaje


    private void Awake() {
        toogleIndicator(false);
        toogleWindow(false);
    }


    private void toogleWindow(bool show) {
        window.SetActive(show);
    }

    public void toogleIndicator(bool show) {
        indicator.SetActive(show);
    }

    //Iniciar dialogo
    public void StartDialogue() {

        if (started) { //para que no se reinicie el dialogo si ya estamos en uno
            return;
        }
        
        started = true; //ha comenzado el dialogo
        toogleWindow(true); // mostrar la ventana
        toogleIndicator(false); // ocultar el indicador
        GettingDialogues(0); // se comienza a mostrar el primer dialogo
    }


    private void GettingDialogues(int i) {
        index = i; // empezar el index a cero
        charIndex = 0; // las frases empezaran en el caracter 0
        dialogueText.text = string.Empty; // vaciamos el componente de texto
        StartCoroutine(type());  // comenzar a escribir e dialogo
    }

    // finalizar dialogo
    public void EndDialogue() {
        started = false; // desactivamos el bool para que vuelva a poder hablar con el player
        waitForNextDialog = false;  
        StopAllCoroutines(); // finalizamos todas las corutinas
        toogleWindow(false); // ocultar la ventana
        //toogleIndicator(true); // mostramos el indicador

    }

    // logica de como aparece el texto de los dialogos
    IEnumerator type() {
        /*yield return new WaitForSeconds(2f); // retraso del texto en ser mostrado
        foreach (char letter in sentences[index].ToCharArray()) { // para cada caracter de la cadena de string de la variable sentences
            dialogText.text += letter;
            yield return new WaitForSeconds(typingSpeed); // permite controlar la velocidad del texto mostrado por el editor de unity
        }*/
        yield return new WaitForSeconds(writingSpeed);// esperamos x segundos
        string currentDialogue = dialogues[index];
        dialogueText.text += currentDialogue[charIndex]; // escribimos los caracteres
        charIndex++; // incrementamos charIndex
        if (charIndex < currentDialogue.Length) { // determinamos si hemos llegado al final de la frase
            yield return new WaitForSeconds(writingSpeed);// esperamos x segundos
            StartCoroutine(type()); // reiniciamos el proceso
        }
        else {
            waitForNextDialog = true; 
        }
    }

    private void Update() {

        if (started==false) {
            return;
        }
        if (waitForNextDialog == true && Input.GetButtonDown("Fire3")) { // el usuario debe pulsar la tecla fire 3 que es la de la rueda del raton o alt izquierdo
            waitForNextDialog = false;
            index++; // incrementamos el index para que muestre la siguiente frase.
            if (index < dialogues.Count) { // comprobamos si tenemos mas frases del NPC
                GettingDialogues(index);
            }
            else {
                toogleIndicator(true);
                EndDialogue(); // terminamos el dialogo
            }
            

        }
    }

}
