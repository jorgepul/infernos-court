using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialog : MonoBehaviour
{

    public TextMeshProUGUI dialogText;
    public string[] sentences;
    private int index;
    public float typingSpeed; 

    IEnumerator type () {
        yield return new WaitForSeconds(2f); // retraso del texto en ser mostrado
        foreach (char letter in sentences[index].ToCharArray()) { // para cada caracter de la cadena de string de la variable sentences
            dialogText.text += letter;
            yield return new WaitForSeconds(typingSpeed); // permite controlar la velocidad del texto mostrado por el editor de unity
        }
    
    }
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(type());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
