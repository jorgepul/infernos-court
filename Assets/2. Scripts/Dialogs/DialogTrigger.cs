using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{

    private bool playerDetected; // determinara si ha detectado o no al player
    public DialogSystem dialogueScript; // accedemos a las funciones de la clase dialogSystem que controlan la logica de las conversaciones.

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            playerDetected = true; // se ha detectado al player
            dialogueScript.toogleIndicator(playerDetected);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)  {
        if (collision.CompareTag("Player"))  {
            playerDetected = false; // se ha dejado de detectar al player
            dialogueScript.toogleIndicator(playerDetected);
            dialogueScript.EndDialogue(); // si el player se aleja del NPC se termina la conversacion
        }
    }


    


    private void Update()
    {
        if (playerDetected && Input.GetButtonDown("Fire3")) { // mientras el trigger este activo se podra interactuar con el NPC
            dialogueScript.StartDialogue(); // comenzamos el dialogo


        }
    }


}
