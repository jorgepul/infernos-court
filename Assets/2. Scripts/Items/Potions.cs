using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Potions : MonoBehaviour
{

    public float healthToGive; // determinara la cantidad de puntos de salud que recobrara el Player al coger una pocion roja
    public float magicToGive; // determinara la cantidad de puntos de magia que recobrara el Player al coger una pocion verde
    //public float expToGive; // determinara la cantidad de puntos de exp que recobrara el Player al coger una pocion azul

    public GameObject itemToAdd; // item de pociones que se va a inventariar.
    public int numberOfItemsToAdd; // numero de items que se incorporaran

    InventaryScript inventory;

    SingletonScript gameManager; 

    private void OnTriggerEnter2D(Collider2D collision){
        if (collision.CompareTag("Player")) { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            //ExperienceSystem.instance.ExpCalculator(GetComponent<Potions>().expToGive); // sumo la exp a la del Player
            AudioManager.instance.audioToPlay(AudioManager.instance.potion); // se ejecutara el sonido correspondiente al interaccionar
            inventory.CheckEmptySlots(itemToAdd, itemToAdd.name, numberOfItemsToAdd); // creamos la pocion en el inventario
            Destroy(gameObject);
        }



        #region old potions script version
        /*
         if (collision.CompareTag("Player")) { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            collision.GetComponent<PlayerHealth>().health += healthToGive; // sumo a la vida del Player la cantidad de vida que otorga una pocion roja
            collision.GetComponent<PlayerHealth>().mana += magicToGive; // sumo el mana al player la cantidad de mana que otorga una pocion verde
            ExperienceSystem.instance.ExpCalculator(GetComponent<Potions>().expToGive); // sumo la exp a la del Player
            AudioManager.instance.audioToPlay(AudioManager.instance.potion); // se ejecutara el sonido correspondiente al interaccionar
            Destroy(gameObject);
        }
         */
        #endregion
    }

    

    private void Start() {
        gameManager = SingletonScript.instance;
        inventory = gameManager.GetComponent<InventaryScript>();
    }


}
