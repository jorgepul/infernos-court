using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // nos permitira controlar los elementos del game controler o UI (user interface)

public class Hearts : MonoBehaviour
{
    public int heartsInTotal; // variable que recoge los corazones que recogera Player
    public Text heartText; // texto que reflejara la cantidad de corazones en la UI

    public int maxHearts; // numero maximo de corazones 

    // Patron singleton
    public static Hearts instance;

    private void Awake()
    {
        if (instance == null) { //
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start(){
        heartText.text = "x " + heartsInTotal.ToString();
    }
    
    public void subItem(int weaponInTotal){
        weaponInTotal += weaponInTotal;

        if (heartsInTotal > maxHearts) { // si la cantidad de corazones es mayor que la maxima, este valor no aumentara por muchos corazones que obtengamos
            heartsInTotal = maxHearts; 
        }

        heartText.text = "x " + weaponInTotal.ToString();
    }
}
