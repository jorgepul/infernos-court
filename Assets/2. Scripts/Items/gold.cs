using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gold : MonoBehaviour
{

    public float moneyFromItems; // determina la cantidad de dinero que tiene el objeto gold

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            totalMoney.instance.money(moneyFromItems); // usara el valor del objeto gold para aumentar el total de dinero que tiene el player
            AudioManager.instance.audioToPlay(AudioManager.instance.gold); // se ejecutara el sonido correspondiente al interaccionar
            Destroy(gameObject);
        }
    }

}
