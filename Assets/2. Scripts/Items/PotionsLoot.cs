using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionsLoot : MonoBehaviour
{
    // Esta clase controla las pociones que dejaran caer los enemigos y que tb podran encontrarse por la pantalla
    
    public float healthToGive; // determinara la cantidad de puntos de salud que recobrara el Player al coger una pocion roja
    public float magicToGive; // determinara la cantidad de puntos de magia que recobrara el Player al coger una pocion verde
    public float expToGive; // determinara la cantidad de puntos de exp que recobrara el Player al coger una pocion azul

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            collision.GetComponent<PlayerHealth>().health += healthToGive; // sumo a la vida del Player la cantidad de vida que otorga una pocion roja
            collision.GetComponent<PlayerHealth>().mana += magicToGive; // sumo el mana al player la cantidad de mana que otorga una pocion verde
            ExperienceSystem.instance.ExpCalculator(GetComponent<PotionsLoot>().expToGive); // sumo la exp a la del Player
            AudioManager.instance.audioToPlay(AudioManager.instance.potion); // se ejecutara el sonido correspondiente al interaccionar
            Destroy(gameObject);
        }
    }
}
