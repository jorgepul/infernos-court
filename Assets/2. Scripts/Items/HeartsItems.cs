using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartsItems : MonoBehaviour
{

    public int heartsToAdd; // a�ade las armas secundarias arrojadizas

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && isTaken==false)
        { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            Hearts.instance.subItem(heartsToAdd); // usara el valor del objeto secondweapon para aumentar el total de armas secundarias que tiene el player
            isTaken = true;
            Destroy(gameObject);
        }
    }

    // para evitar que el valor del objeto se duplique me aseguro que solo sea tomado 1 vez
    public bool isTaken; // determinara si el objeto ha sido tomado o no

 
}
