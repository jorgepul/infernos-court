using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePotions : MonoBehaviour
{
    public float expToGive; // determinara la cantidad de puntos de exp que recobrara el Player al coger una pocion azul

    private void OnTriggerEnter2D(Collider2D collision)
    {
         if (collision.CompareTag("Player")) { // si se produce una colision con el colider del elemento que tenga el tag "Player"
            ExperienceSystem.instance.ExpCalculator(GetComponent<BluePotions>().expToGive); // sumo la exp a la del Player
            AudioManager.instance.audioToPlay(AudioManager.instance.potion); // se ejecutara el sonido correspondiente al interaccionar
            Destroy(gameObject);
        }

    }

}
