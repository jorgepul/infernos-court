using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{

    public GameObject itemToAdd; // item de pociones que se va a inventariar.
    public int numberOfItemsToAdd; // numero de items que se incorporaran
    InventaryScript inventory;
    SingletonScript gameManager;

    bool isTaken; // para evitar errores de conteo al recoger objetos

    // Start is called before the first frame update
    void Start()
    {
        gameManager = SingletonScript.instance;
        inventory = gameManager.GetComponent<InventaryScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player")) {
            if (!isTaken){ // si no ha sido cogido el objeto
                AudioManager.instance.audioToPlay(AudioManager.instance.potion); // se ejecutara el sonido correspondiente al interaccionar
                inventory.CheckEmptySlots(itemToAdd, itemToAdd.name, numberOfItemsToAdd); // creamos la pocion en el inventario
                isTaken = true; // determinamos que el objeto ha sido recogido
                Destroy(gameObject);

            }
        }

    }
}
