using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UsingItems : MonoBehaviour
{
    public float healthToGive; // determinara la cantidad de puntos de salud que recobrara el Player al coger una pocion roja
    public float magicToGive; // determinara la cantidad de puntos de magia que recobrara el Player al coger una pocion verde
    //public float expToGive; // determinara la cantidad de puntos de exp que recobrara el Player al coger una pocion azul

    public float strRise; // determinara la cantidad de fuerza que adquiere el Player al coger el item
    public float defRise; // determinara la cantidad de defensa que adquiere el Player al coger el item
    public float attckRise; // determinara la cantidad de ataque que adquiere el Player al coger el item

    public void UseItem() {
        

        if (gameObject.name == "SuperRedPotion (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().health += healthToGive;
        }

        if (gameObject.name == "Elixir (inventary)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().health += healthToGive;
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().mana += magicToGive;
        }

        if (gameObject.name == "GreenPotion (inventary)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().mana += magicToGive;
        }

        if (gameObject.name == "RedPotion (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().health += healthToGive;
        }

        if (gameObject.name == "Apple (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().health += healthToGive;
        }

        if (gameObject.name == "SuperGreenPotion (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().mana += magicToGive;
        }

        if (gameObject.name == "UltraGreenPotion (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().mana += magicToGive;
        }

        if (gameObject.name == "UltraRedPotion (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerHealth>().health += healthToGive;
        }

        // LAS POCIONES AZULES SE PONEN APARTE Y NO SE INVENTARIAN PUES NO SE CONSIGUE QUE FUNCIONE BIEN CORRECTAMENTE POR ESTE METODO
        // algunos enemigos las soltaran y se consumen en el acto
    }


    public void RiseStatsItem()
    {
        if (gameObject.name == "StrRise (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerControler>().strenght += strRise;
        }

        if (gameObject.name == "DefRise (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerControler>().defense += defRise;
        }

        if (gameObject.name == "AtkRise (inventory)")
        {
            PlayerControler.instance.gameObject.GetComponent<PlayerControler>().attackWeapon += attckRise;
        }


    }
}
