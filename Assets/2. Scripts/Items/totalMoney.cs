using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // nos permitira controlar los elementos del game controler o UI (user interface)

public class totalMoney : MonoBehaviour
{
    public float moneyInTotal; // variable que recoge el dinero que recogera Player
    public Text moneyText; // texto que reflejara la cantidad de dinero en la UI

    // Patron singleton
    public static totalMoney instance;

    private void Awake(){
        if (instance == null){ //
            instance = this;
        }
    }

    private void Start() {
        moneyInTotal = PlayerPrefs.GetFloat("Gold", moneyInTotal);
        moneyText.text = "x " + moneyInTotal.ToString(); 
    }


    public void money(float moneyFromItems){

        
        moneyInTotal += moneyFromItems; // el valor del objeto dinero se sumara al total del que dispone el jugador

        SaveSystem.instance.GOLDSavingData(moneyInTotal);
        moneyInTotal = PlayerPrefs.GetFloat("Gold");

        moneyText.text = "x " + moneyInTotal.ToString(); //aparecera en la UI el texto x mas la cantidad acumulada
    }

}
