using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyHealth : MonoBehaviour
{
    Enemy enemy;
    public bool isDamaged;
    public GameObject deathEffect;
    SpriteRenderer sprite;
    Blink material;
    Rigidbody2D rb;

    public float primeLife ; // recogera el valor de la vida del enemigo para regresarsela al resucitar.

    public GameObject damageText; // mostrara el da�o en la pantalla cuando el enemigo lo reciva


    private void Start()
    {
        enemy = GetComponent<Enemy>();
        primeLife = GetComponent<Enemy>().healthPoints;
        sprite = GetComponent<SpriteRenderer>();
        material = GetComponent<Blink>();
        rb = GetComponent<Rigidbody2D>();

    }


    // Metodos para controlar la salud del enemigo cada vez que el collider del arma del Player contacte con el collider del Enemy
    // y no ha sido da�ado por el arma una vez por contacto
    private void OnTriggerEnter2D(Collider2D collision)
    {

        
        if (collision.CompareTag("Weapon") && !isDamaged)
        {
            enemy.healthPoints -= enemy.damageReceived; // la vida la ira perdiendo segun lo establecido en el calculo
            //enemy.healthPoints -= collision.GetComponent<EnemyHealth>().DamageInput(enemy.defense);
            GameObject textGo = Instantiate(damageText, transform.position, Quaternion.identity);
            StartCoroutine(textAbove(textGo)); // corrutina para desplazar el texto sobre el enemigo
            textGo.GetComponent<TextMeshPro>().SetText(enemy.damageReceived.ToString());
            
            AudioManager.instance.audioToPlay(AudioManager.instance.enemyHitted); // se ejecutara el sonido correspondiente al interaccionar

            //enemy.healthPoints -= 1f; // la vida la ira perdiendo de punto en punto al ser golpeado

            // para evitar que el enemigo al ser golpeado se desplace hacia atras siempre en el eje X positivo
            // de modo que si el Player esta ala izquierda el enemigo se ira hacia la derecha y viceversa
            if (collision.transform.position.x < transform.position.x)
            {
                rb.AddForce(new Vector2(enemy.knockBackForceX, enemy.knockBackForceY), ForceMode2D.Force); // hacia la derecha
            }
            else
            {
                rb.AddForce(new Vector2(-enemy.knockBackForceX, enemy.knockBackForceY), ForceMode2D.Force); // hacia la izquierda
            }

            StartCoroutine(Damager());
            if (enemy.healthPoints <= 0) {
               
                Instantiate(deathEffect, transform.position, Quaternion.identity); // esto permite a la animacion de EnemyDeath activarse cuando la salud llegue a 0
                ExperienceSystem.instance.ExpCalculator(GetComponent<Enemy>().expToGive); // entregara la experiencia del enemigo al player
                AudioManager.instance.audioToPlay(AudioManager.instance.enemyDeath); // se ejecutara el sonido correspondiente al interaccionar

                // loot con % de aparicion
                int lootRatio = Random.Range(0,100); // indicara la probabilidad de aparicion del objeto

                if (lootRatio <= 1)
                {
                    Instantiate(enemy.lootItems[2].gameObject, transform.position, Quaternion.identity); // se creara el objeto loot al morir - objeto raro mitico

                }
                else if (lootRatio > 1 && lootRatio <= 25)
                {
                    Instantiate(enemy.lootItems[1].gameObject, transform.position, Quaternion.identity); // se creara el objeto loot al morir - objeto raro
                }
                else {
                    Instantiate(enemy.lootItems[0].gameObject, transform.position, Quaternion.identity); // se creara el objeto loot al morir - objeto comun
                }

                /*
                 * version 2 objetos loot
                if (lootRatio > 10)
                {
                    Instantiate(enemy.lootItems[0].gameObject, transform.position, Quaternion.identity); // se creara el objeto loot al morir
                }
                else {
                    Instantiate(enemy.lootItems[1].gameObject, transform.position, Quaternion.identity); // se creara el objeto loot al morir
                }
                */

                // respawn de enemigos
                if (enemy.enemyRespawn == true) {
                    // scrip respawn para resucitar a los enemigos; el script ira con getcomponent in parent porque respawn scrips va en la clase padre
                    transform.GetComponentInParent<RespawnEnemies>().StartCoroutine(GetComponentInParent<RespawnEnemies>().resurrectEnemy());
                }
                else {
                    Destroy(gameObject); // el enemigo sera destruido con la animacion de muerte
                }


            }
        }
        
    }
    
    // metodo para cercionarnos de que unicamente el arma haga da�o cada vez que se pulsa Attack en lugar de varias 
    IEnumerator Damager()
    {
        isDamaged = true;
        sprite.material = material.blink; // al ser golpeado cambia la "animacion" por la de golpeado
        yield return new WaitForSeconds(0.5f);
        isDamaged = false;
        sprite.material = material.original; // pasados los 0.5f segundos volvera al sprite original
    }

    // el texto de no tener la siguiete corutina saldra dentro del gameobject Enemy y su visualizacion seria dificil
    // por ello moveremos el texto para que quede por encima del enemigo hacia arriba hasta desaparecer
    IEnumerator textAbove(GameObject text) {
        Vector2 initPos = new Vector2(text.transform.position.x, text.transform.position.y); // marcamos la posicion inicial de donde sale el texto
        Vector2 finalPos = new Vector2(text.transform.position.x, text.transform.position.y-5); // dejamos el eje x como estaba y aumentamos el eje y 20 unidades
        int upTimes = 0; // veces que ejecutara el while
        while (upTimes < 6) { //mientras el upTimes sea menor de 6 se correra el bucle
            upTimes++; // incrementamos el upTimes
            text.transform.position = Vector2.MoveTowards(initPos, finalPos, 15f * Time.deltaTime); // el gameobject del texto se movera hasta la posicion final
            yield return new WaitForEndOfFrame(); // cada vez que pase un frame se vuelve a correr el while
        }
    
    
    }

}
