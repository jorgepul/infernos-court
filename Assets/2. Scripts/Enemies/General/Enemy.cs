using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using TMPro;

public class Enemy : MonoBehaviour
{

    public string enemyName; // nombre del enemigo
    public float healthPoints; // vida del enemigo
    public float strenght; // medira el valor de la fuerza del enemigo
    public float defense; // medira el valor de la defensa del enemigo
    public float speed; // velocidad del enemigo
    public float attack; // medira el ataque del enemigo hacia el Player

    public float expToGive; // medira la cantidad de experiencia que otorga el enemigo al ser eliminado
    

    public float knockBackForceX; // medira el retroceso en el eje X del enemigo al ser golpeado
    public float knockBackForceY; // medira el retroceso en el eje Y del enemigo al ser golpeado


    public float damageReceived; // medira el da�o que recibe el enemigo en base a las estadisticas del player y del enemigo

    //public GameObject damageText; // texto que aparecera en pantalla al golpear enemigos

    // En juegos como castlevania el respawn es de sala a sala, en blasphemous es hasta que el jugador guarde la partida o se cure en un santuario
    // optamos por la segunda via
    public bool enemyRespawn; // determinara si el enemigo debe volver a aparecer


    // sistema loot para los enemigos
    public GameObject[] lootItems; 

    private void Update() {
        // este campo sera actualizado incluso cuando suba el player de nivel
        damageReceived =  PlayerControler.instance.attackWeapon + PlayerControler.instance.strenght - defense ;
        // el da�o recibido sera igual ataque del arma, fuerza del jugador menos la defensa del enemigo
        //DamageShow();
    }


    // patron singleton para actualizar al enemigo
    public static Enemy instance; // instancia de clase
    private void Awake() {
        if (instance == null)
        {
            instance = this;
        }
    }


    /*
    public void DamageShow() { // Mostrara el da�o por pantalla
        GameObject textScreen = Instantiate(damageText, transform.position, quaternion.identity); // cada vez que golpemos a un enemigo sera considerado como una instancia
        textScreen.GetComponent<TextMeshPro>().SetText(damageReceived.ToString()); // convertira el valor de damageReceived a string
        print(damageReceived); // mostramos el valor de damageReceived
    }
    */
}
