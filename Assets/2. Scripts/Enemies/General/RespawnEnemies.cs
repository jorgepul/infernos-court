using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnEnemies : MonoBehaviour
{
    // esta clase determinara el tiempo que tarda en regernerarse el enemigo
    // esto se pondra dentro del "padre" del tipo monstruo pero no en los bosses

    public float timeToReurrect; // tiempo que hay que esperar para que el enemigo vuelva a aparecer
    //public float cdToResurrect;
    public GameObject enemyToResurrect;

    public bool isResurrecting; 
  

    // Start is called before the first frame update
    void Start()
    {
        enemyToResurrect = transform.GetChild(0).gameObject; // determinara que el enemigo es el hijo numero 0 de modo que asi no tendremos que ir asignandolo por cada enemigo
        /*
         de este modo los enemigos deberan crearse siguiendo el patron siguiente dentro del proyecto
        Skeleton
            Skeleton -> este sera el child 0 y tendra los scripts de enemy, movements, health
                wallcheck
                pitcheck
                ...
            PointA
            PointB
            ...
         */
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator resurrectEnemy()
    {
        enemyToResurrect.SetActive(false); // hasta que no muera el enemigo estamos con el bool en balso
        yield return new WaitForSeconds(timeToReurrect); // se espera x de tiempo
        enemyToResurrect.SetActive(true); // resucitaremos al enemigo para que vuelva a por nosotros
        enemyToResurrect.GetComponent<Enemy>().healthPoints = 
            enemyToResurrect.GetComponent<EnemyHealth>().primeLife; // el enemigo resucitado tendra la vida original
        enemyToResurrect.GetComponent<SpriteRenderer>().material = enemyToResurrect.GetComponent<Blink>().original; // dejara de estar en el estado de efecto de da�o
        enemyToResurrect.GetComponent<EnemyHealth>().isDamaged = false; // determinamos que una vez resucitado pueda volver a ser da�ado
        yield return ResurrectionAnim(); 

    }

    IEnumerator ResurrectionAnim() { // permitira saber si el enemigo esta resucitando
        isResurrecting = true;
        enemyToResurrect.GetComponent<Animator>().SetBool("Resurrecting", true); // activamos la animacion de resurreccion como verdadera
        yield return new WaitForSeconds(0.5f); // esperamos 0.5 s 
        enemyToResurrect.GetComponent<Animator>().SetBool("Resurrecting", false); // activamos la animacion de resurreccion como falsa
        isResurrecting = false; // ya habra resucitado
    }
}
