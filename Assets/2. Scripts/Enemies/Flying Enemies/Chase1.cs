using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase1 : MonoBehaviour
{

    public Fly[] enemyArray;


    // cuando el player entre en la seccion de busqueda la variable ischasing cambia y se inicia la persecucion
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            foreach (Fly enemy in enemyArray)
            {
                enemy.ischasing = true;
                // enemy.FindPlayer(collision.gameObject);
            }
        }
    }

    // cuando el player salga en la seccion de busqueda la variable ischasing cambia y se deja la persecucion
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {

            foreach (Fly enemy in enemyArray)
            {
                enemy.ischasing = false;
            }
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
