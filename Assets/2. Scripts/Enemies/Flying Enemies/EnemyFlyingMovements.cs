using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlyingMovements : MonoBehaviour
{
    float flyingSpeed; // controla la velocidad de los enemigos
    float velX;
    float velY;
    Rigidbody2D fliyingRb;
    Animator fliyingAnim;
    public bool flyingIsStatic; // determinara a un enemigo que no se va a mover
    public bool flyingIsWalker; // determinara a un enemigo que se desplaza
    public bool flyingWalksRight; // determinara si el enemigo se desplaza hacia la derecha
    //public Transform pitCheck; // determinara si es el borde de una plataforma
    public Transform flyingWallCheck; // determinara si el objeto es una pared del nivel
                                      // public Transform groundCheck; // determinara si el enemigo esta en contacto con el suelo del nivel

    //public bool pitDetected; // determinara si se ha encontrado un borde de plataforma
    public bool flyingWallDetected; // determinara si se ha encontrado un muro
    //public bool groundDetected; // determinara si se ha encontrado suelo

    public float flyingDetectionRadious;
    public LayerMask flyingWhatIsGround; //identificara si el elemento de contacto del enemigo es un muro, borde, suelo...

    public bool flyingIsPatrolling; // determinara si el enemigo esta patrullando
    public Transform flyingPointA; // determinara el punto A sobre el que realiza la accion de patrullar
    public Transform flyingPointB; // determinara el punto B sobre el que realiza la accion de patrullar
    public bool flyingGoToA; // determina si el enemigo se dirige hacia el punto A; si flyingGoToA > 0 el enemigo se desplaza hacia A
    public bool flyingGoToB; // determina si el enemigo se dirige hacia el punto B; si flyingGoToB > 0 el enemigo se desplaza hacia B
    public bool flyingWaiting; // determinara una pausa dentro del tiempo de patrulla
    public float flyingWaitingTime; // tiempo de pausa dentro de la patrulla antes de iniciar el trayecto al otro punto.
    public bool isFlyingWaiting;


    // Start is called before the first frame update
    void Start()
    {
        flyingSpeed = GetComponent<Enemy>().speed;
        fliyingRb = GetComponent<Rigidbody2D>();
        fliyingAnim = GetComponent<Animator>();
        flyingGoToA = true;
    }

    // Update is called once per frame
    void Update()
    {
        // se activara cuando no este detectando el suelo de modo que nos encontraremos con un borde
        //pitDetected = !Physics2D.OverlapCircle(pitCheck.position, flyingDetectionRadious, flyingWhatIsGround);
        flyingWallDetected = Physics2D.OverlapCircle(flyingWallCheck.position, flyingDetectionRadious, flyingWhatIsGround); // se activara cuando este detectando el muro de modo que nos encontraremos con un muro

        if (flyingWallDetected)
        {
            Flip(); // cuando encuentre un borde o una pared y si se encuentra en la plataforma, se volteara hacia el otro lado y seguira caminando
            // si no esta en contacto con una plataforma no se ejecutara la fliyingAnimacion
        }

        // solventamos fallos en la fliyingAnimacion del enemigo en caso de que se quede en el aire
        //groundDetected = Physics2D.OverlapCircle(groundCheck.position, flyingDetectionRadious, flyingWhatIsGround);

    }


    private void FixedUpdate()
    {
        // determinaremos el movimiento de un enemigo estatico
        if (flyingIsStatic)
        { //si el enemigo es estatico asignamos el valor de true a la fliyingAnimacion Idle
            fliyingAnim.SetBool("Idle", true);
            fliyingRb.constraints = RigidbodyConstraints2D.FreezeAll; // evitaremos asi que nuestro personaje desplaze al enemigo estatico por una colision
        }
        // determinaremos el movimiento de un enemigo que se desplaza
        if (flyingIsWalker)
        {
            fliyingRb.constraints = RigidbodyConstraints2D.FreezeRotation; // evitaremos que el enemigo rote al moverse
            fliyingAnim.SetBool("Idle", false);
            if (!flyingWalksRight)
            {
                fliyingRb.velocity = new Vector2(-flyingSpeed * Time.deltaTime, fliyingRb.velocity.y); // desplazamiento hacia la izquierda
            }
            else
            {
                fliyingRb.velocity = new Vector2(flyingSpeed * Time.deltaTime, fliyingRb.velocity.y); // desplazamiento hacia la derecha
            }

        }
        // determinaremos el movimiento de un enemigo que patrulla
        if (flyingIsPatrolling)
        {
            fliyingAnim.SetBool("Idle", false);
            if (flyingGoToA == true)
            { // si se desplaza hacia A

                if (!isFlyingWaiting)
                {
                    fliyingAnim.SetBool("Idle", false);
                    fliyingRb.velocity = new Vector2(-flyingSpeed * Time.deltaTime, fliyingRb.velocity.y); // el punto A ira a la izq de modo que se desplazara hacia la izq

                }
                if (Vector2.Distance(transform.position, flyingPointA.position) < 0.2f)
                {
                    if (flyingWaiting)
                    {
                        StartCoroutine(FlyingWaiting()); // invocamos a la funcion FlyingWaiting
                    }

                    Flip(); // cuando llegue al punto A girara
                    flyingGoToA = false;
                    flyingGoToB = true;
                }
            }
            if (flyingGoToB == true)
            {
                if (!isFlyingWaiting)
                {
                    fliyingAnim.SetBool("Idle", false);
                    fliyingRb.velocity = new Vector2(flyingSpeed * Time.deltaTime, fliyingRb.velocity.y); // el punto B ira a la derecha de modo que se desplazara hacia la derecha

                }
                if (Vector2.Distance(transform.position, flyingPointB.position) < 0.2f)
                {
                    if (flyingWaiting)
                    {
                        StartCoroutine(FlyingWaiting()); // invocamos a la funcion FlyingWaiting
                    }

                    Flip(); // cuando llegue al punto B girara
                    flyingGoToA = true;
                    flyingGoToB = false;
                }
            }
        }
    }

    public void Flip()
    {
        flyingWalksRight = !flyingWalksRight; // si no camina hacia la derecha lo hara hacia la izquierda y viceversa  
        transform.localScale *= new Vector2(-1, transform.localScale.y); // multiplicamos por -1 la escala del eje x para que se de la vuelta
    }

    IEnumerator FlyingWaiting()
    { // al llegar a uno de los puntos de patrulla el enemigo dejara de caminar y pasara a Idle una cantidad de segundos
        fliyingAnim.SetBool("Idle", true);
        isFlyingWaiting = true;
        Flip(); // cuando llegue al punto girara
        yield return new WaitForSeconds(flyingWaitingTime);
        isFlyingWaiting = false;
        fliyingAnim.SetBool("Idle", false);
        Flip(); // cuando llegue al punto girara
    }




}
