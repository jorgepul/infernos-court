using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    public float speed; // velocidad del movimiento del enemigo volador
    private GameObject player; // determinara si se detecta al jugador
    public bool ischasing = false; // determinamos que el enemigo de partida no esta persiguiendo al player a menos que lo detecte
    public Transform initialPoint; // determina la posicion original del enemigo volador

    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        //encontrar al jugador por su etiqueta
        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (player == null)
        {
            return; //cuando no detecte al player volvera a su posicion de partida
        }
        
        if (ischasing == true)
        { //el enemigo ha detectado al player e inicia la persecucion
            anim.SetBool("Walk", true);  //si el enemigo volador detecta al player se inicia la animacion de walk (modelo persecucion)
            Chase();
        }
        else
        {
            // el enemigo volvera a la casilla de espera
            anim.SetBool("Walk", false);  //si el enemigo volador ha perdido al player se cancela la animacion de walk (modelo persecucion)
            BacktoIdle(); // vuelve al punto de espera
        }
        //Chase();
        Flip();
    }

    public void Chase()
    {
        // esta funcion permitira perseguir al player hasta que el enemigo volador muera
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        if (Vector2.Distance(transform.position, player.transform.position) <= 0.5)
        {
            //aqui podriamos incluir alguna funcion del tipo ataque/magia...
        }
        else { 
        // reseteamos las variables.
        }
    }

    public void Flip()
    { // rotaremos al enemigo volador en relacion a la posicion del player
        if (transform.position.x > player.transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }
        else
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    private void BacktoIdle()
    { //funcion que devuelve al enemigo a donde estaba
        transform.position = Vector2.MoveTowards(transform.position, initialPoint.position, speed * Time.deltaTime);
    }
}
