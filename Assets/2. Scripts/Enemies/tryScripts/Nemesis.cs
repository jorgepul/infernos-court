using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Nemesis : MonoBehaviour
{

    public AIPath aiPath; 



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // girar el sprite de nemesis para que tenga al Player siempre a la vista
        if (aiPath.desiredVelocity.x >= 0.01f) { // buscara el enemigo el cual esta a la derecha de nemesis
            transform.localScale = new Vector3(-1f, 1f, 1f);
        } else if (aiPath.desiredVelocity.x <= - 0.01f) { // buscara el enemigo el cual esta a la izquierda de nemesis
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
    }
}
