using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovements : MonoBehaviour
{

    float speed; // controla la velocidad de los enemigos
    float velX;
    float velY;
    Rigidbody2D rb;
    Animator anim;
    public bool isStatic; // determinara a un enemigo que no se va a mover
    public bool isWalker; // determinara a un enemigo que se desplaza
    public bool walksRight; // determinara si el enemigo se desplaza hacia la derecha
    public Transform pitCheck; // determinara si es el borde de una plataforma
    public Transform wallCheck; // determinara si el objeto es una pared del nivel
    public Transform groundCheck; // determinara si el enemigo esta en contacto con el suelo del nivel

    public bool pitDetected; // determinara si se ha encontrado un borde de plataforma
    public bool wallDetected; // determinara si se ha encontrado un muro
    public bool groundDetected; // determinara si se ha encontrado suelo

    public float detectionRadious;
    public LayerMask whatIsGround; //identificara si el elemento de contacto del enemigo es un muro, borde, suelo...

    public bool isPatrolling; // determinara si el enemigo esta patrullando
    public Transform pointA; // determinara el punto A sobre el que realiza la accion de patrullar
    public Transform pointB; // determinara el punto B sobre el que realiza la accion de patrullar
    public bool goToA; // determina si el enemigo se dirige hacia el punto A; si goToA > 0 el enemigo se desplaza hacia A
    public bool goToB; // determina si el enemigo se dirige hacia el punto B; si goToB > 0 el enemigo se desplaza hacia B
    public bool waiting; // determinara una pausa dentro del tiempo de patrulla
    public float waitingTime; // tiempo de pausa dentro de la patrulla antes de iniciar el trayecto al otro punto.
    public bool isWaiting;


    // Start is called before the first frame update
    void Start()
    {
        speed = GetComponent<Enemy>().speed;
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        goToA = true;
    }

    // Update is called once per frame
    void Update()
    {
        // se activara cuando no este detectando el suelo de modo que nos encontraremos con un borde
        pitDetected = !Physics2D.OverlapCircle(pitCheck.position, detectionRadious, whatIsGround);
        wallDetected = Physics2D.OverlapCircle(wallCheck.position, detectionRadious, whatIsGround); // se activara cuando este detectando el muro de modo que nos encontraremos con un muro

        if ((pitDetected || wallDetected) && groundDetected)
        {
            Flip(); // cuando encuentre un borde o una pared y si se encuentra en la plataforma, se volteara hacia el otro lado y seguira caminando
            // si no esta en contacto con una plataforma no se ejecutara la animacion
        }

        // solventamos fallos en la animacion del enemigo en caso de que se quede en el aire
        groundDetected = Physics2D.OverlapCircle(groundCheck.position, detectionRadious, whatIsGround);

    }


    private void FixedUpdate()
    {
        // determinaremos el movimiento de un enemigo estatico
        if (isStatic)
        { //si el enemigo es estatico asignamos el valor de true a la animacion Idle
            anim.SetBool("Idle", true);
            rb.constraints = RigidbodyConstraints2D.FreezeAll; // evitaremos asi que nuestro personaje desplaze al enemigo estatico por una colision
        }
        // determinaremos el movimiento de un enemigo que se desplaza
        if (isWalker)
        {
            rb.constraints = RigidbodyConstraints2D.FreezeRotation; // evitaremos que el enemigo rote al moverse
            anim.SetBool("Idle", false);
            if (!walksRight)
            {
                rb.velocity = new Vector2(-speed * Time.deltaTime, rb.velocity.y); // desplazamiento hacia la izquierda
            }
            else
            {
                rb.velocity = new Vector2(speed * Time.deltaTime, rb.velocity.y); // desplazamiento hacia la derecha
            }

        }
        // determinaremos el movimiento de un enemigo que patrulla
        if (isPatrolling)
        {
            anim.SetBool("Idle", false);
            if (goToA == true)
            { // si se desplaza hacia A

                if (!isWaiting)
                {
                    anim.SetBool("Idle", false);
                    rb.velocity = new Vector2(-speed * Time.deltaTime, rb.velocity.y); // el punto A ira a la izq de modo que se desplazara hacia la izq

                }
                if (Vector2.Distance(transform.position, pointA.position) < 0.2f)
                {
                    if (waiting)
                    {
                        StartCoroutine(Waiting()); // invocamos a la funcion Waiting
                    }

                    Flip(); // cuando llegue al punto A girara
                    goToA = false;
                    goToB = true;
                }
            }
            if (goToB == true)
            {
                if (!isWaiting)
                {
                    anim.SetBool("Idle", false);
                    rb.velocity = new Vector2(speed * Time.deltaTime, rb.velocity.y); // el punto B ira a la derecha de modo que se desplazara hacia la derecha

                }
                if (Vector2.Distance(transform.position, pointB.position) < 0.2f)
                {
                    if (waiting)
                    {
                        StartCoroutine(Waiting()); // invocamos a la funcion Waiting
                    }

                    Flip(); // cuando llegue al punto B girara
                    goToA = true;
                    goToB = false;
                }
            }
        }
    }

    public void Flip()
    {
        walksRight = !walksRight; // si no camina hacia la derecha lo hara hacia la izquierda y viceversa  
        transform.localScale *= new Vector2(-1, transform.localScale.y); // multiplicamos por -1 la escala del eje x para que se de la vuelta
    }

    IEnumerator Waiting()
    { // al llegar a uno de los puntos de patrulla el enemigo dejara de caminar y pasara a Idle una cantidad de segundos
        anim.SetBool("Idle", true);
        isWaiting = true;
        Flip(); // cuando llegue al punto girara
        yield return new WaitForSeconds(waitingTime);
        isWaiting = false;
        anim.SetBool("Idle", false);
        Flip(); // cuando llegue al punto girara
    }


}
