using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    // de termina un nombre generico para el objeto lanzable pues seran o lanzas o flechas...
    // en los comentarios se referencia a lanza pues el esquema se hizo con dicho objeto.
    public GameObject missile; // objeto que lanzara el enemigo
    public float shootingTime; // tiempo de lanzar el misil
    public float shootingLoad; // tiempo que que espera al lanzar una nueva lanza
    public bool freqShotter; // determinara si el enemigo es estatico y dispara continuamente
    public bool isWatcher; // determinara si el enemigo es estatico y esta a la espera de localizar al enemigo

    
    
    // Start is called before the first frame update
    void Start()
    {
        shootingLoad = shootingTime;
    }

    // Update is called once per frame
    void Update() {
        if (freqShotter) { // en caso que el enemigo dispare frecuentemente
            shootingLoad -= Time.deltaTime; // El tiempo real de juego sera el que haga disminuir el tiempo de espera para lanzar la lanza

        }


        if (shootingLoad < 0) { // si el tiempo de recarga es 0 el enemigo podra volver a lanzar la lanza
            Shoot();
        }

    }

    public void Shoot() {
        GameObject missileShot = Instantiate(missile, transform.position, Quaternion.identity);
            //Para enemigos estaticos
            if (transform.localPosition.x < 0) { // la lanza ira hacia la derecha
                missileShot.GetComponent<Rigidbody2D>().AddForce(new Vector2(150f, 0f), ForceMode2D.Force); // determina la fuerza y direccion en que se dispara la lanza
            } else { // la lanza ira hacia la izquierda
                missileShot.GetComponent<Rigidbody2D>().AddForce(new Vector2(-150f, 0f), ForceMode2D.Force); 
            }
        shootingLoad = shootingTime; // reiniciamos la variable para que vuelva a dispara pasado un tiempo

    }

}
