using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LicheBehaviour : MonoBehaviour
{
    //El liche aparecera dentro de las 5 posiciones marcadas de manera aleatoria y atacara con magia
    //Los parametros y caracteristicas han sido homenajeadas a la entrada del Liche en el manual de monstruos de D&D y el libro de jugador

    public Transform[] apparitions; // guardara la posicion de cada punto de las apariciones
    
    // Variables que determinan el ataque
    public GameObject fireBall; // nombre del ataque del liche
    public float castingTime; // tiempo que tardara en lanzar el hechizo desde su aparicion
    public float shortRest; // tiempo que tardara en lanzar otro hechizo
    public float phasingTime; // tiempo que tardara en iniciar el cambio de plano
    public float longRest; // tiempo de espera hasta poder realizar el cambio de planos


    // variables que relacionan la "No" vida del liche
    public float bossHealth; // Cuanta vida tiene el Liche o boss
    public float remainingLife; // Cuanta vida le queda al Liche
    public Image BossHealthBar; // imagen de la barra del jefe para que el Player pueda seguir el combate


    // Start is called before the first frame update
    void Start() {
       // var startingPoint = Random.Range(0, apparitions.Length); // Comenzara en un punto cualquiera del vector apparitions
        transform.position = apparitions[1].position;
        shortRest = castingTime; // reiniciamos el tiempo hasta el proximo hechizo de la bola de fuego
        longRest = phasingTime; // reiniciamos el tiempo hasta el proximo hechizo del cambio de fase
    }

    // Update is called once per frame
    void Update()
    {
        DamageBoss(); //seguimiento en tiempo real de la barra de vida del Boss
        BossLookingOn(); // punto de vista del Boss respecto al Player

        shortRest -= Time.deltaTime; //cuenta atras para volver a lanzar el hechizo de la bola de fuego
        longRest -= Time.deltaTime; //cuenta atras para volver a lanzar el hechizo del cambio de plano

        if (shortRest <= 0f) {
            castingSpell(); // lanza el hechizo
            shortRest = castingTime; // reiniciamos el tiempo hasta el proximo hechizo
            //PlaneShift(); // el Liche cambiara de plano tras atacar
        }

        if (longRest <= 0f) {
            longRest = phasingTime;
            PlaneShift();
        }
    }

    // Un liche es un conjurador de modo que su ataque sera realizando hechizos
    public void castingSpell() {
            GameObject spell = Instantiate(fireBall, transform.position, Quaternion.identity); // creamos una instancia desde la posicion donde aparezca el liche y atacamos sin que haya rotaion del hechizo
    }

    // Cambio de posicion del Liche entre las diferentes opciones
    public void PlaneShift() {
        var startingPoint = Random.Range(0, apparitions.Length); // Comenzara en un punto cualquiera del vector apparitions
        transform.position = apparitions[startingPoint].position;

    }

    public void DamageBoss() {
        remainingLife = GetComponent<Enemy>().healthPoints; // asociamos la vida del script Enemy a la vida del Boss
        BossHealthBar.fillAmount = remainingLife / bossHealth; 
    }

    // El boss este siempre mirando al player a la hora de lanzar el hechizo
    public void BossLookingOn() {
        if (transform.position.x > PlayerControler.instance.transform.position.x)  { // Boss esta a la derecha del Player
            transform.localScale = new Vector3(2, 2, 1); // mirara a la izquierda      
        } else { // Boss a la izquierda del Player
            transform.localScale = new Vector3(-2, 2, 1); // mirara a la derecha 
        }
    }

    /*// Al matar al Liche/Boss se liberara al Player del area de combate
    private void OnDestroy() {
        BossesUI.instance.FinishHim(); 
    }
    */
}
