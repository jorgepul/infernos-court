using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpell : MonoBehaviour
{

    float spellSpeed; //velocidad a la que se desplazara la bola de fuego
    Rigidbody2D rb;
    Vector2 spellDirection;
    PlayerControler target; // el objetivo de la bola de fuego sera el Player
    
 
    // Start is called before the first frame update
    void Start()
    {
        spellSpeed = GetComponent<Enemy>().speed; // recuperamos la velocidad del enemigo que hace el encantamiento
        rb = GetComponent<Rigidbody2D>();
        target = PlayerControler.instance;

        // la bola de fuego tendra en consideracion donde esta el Player y donde esta el Conjurador y se dirigira hacia el
        spellDirection = (target.transform.position - transform.position).normalized * spellSpeed;
        rb.velocity = new Vector2(spellDirection.x, spellDirection.y);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
