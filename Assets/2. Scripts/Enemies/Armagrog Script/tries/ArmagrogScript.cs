using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum ArmagrogStatus { 
IDLE, 
ATTACK,
DEAD,
CAST,
TALKING, // breve conversacion entre el player y Armagrog
SPELL,
WALK

}



public class ArmagrogScript : MonoBehaviour
{


    public ArmagrogStatus status;
    Animator anim;
    public float statusChange;
    
    // Start is called before the first frame update
    void Start()
    {
        status = ArmagrogStatus.IDLE;
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator armagrogStatuses() {
        yield return new WaitForSeconds(statusChange);
    }
}
