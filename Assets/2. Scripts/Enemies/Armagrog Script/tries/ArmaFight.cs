using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArmaFight : MonoBehaviour
{
    [SerializeField] Transform player; // determinara la posicion del Player respecto al enemigo 

    [SerializeField] public float detectionRange; // distancia a la que el enemigo ve al jugador

    public float velocity;


    Rigidbody2D rb;

    Animator anim;

    //public bool walksRight; // determinara si el enemigo se desplaza hacia la derecha
    //public bool facingLeft; // determina si el enemigo esta mirando a la izquierda
    public bool facingRight;


    public float space;


    // variables que relacionan la "No" vida del liche
    public float bossHealth; // Cuanta vida tiene el Liche o boss
    public float remainingLife; // Cuanta vida le queda al Liche
    public Image BossHealthBar; // imagen de la barra del jefe para que el Player pueda seguir el combate

    public GameObject fireBall; // nombre del ataque del liche

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        facingRight = true;
    }

    // Update is called once per frame
    void Update()
    {
        // distancia la jugador
        float spaceToPlayer = Vector2.Distance(transform.position, player.position);
        space = spaceToPlayer;
        Debug.Log("Distancia del Player: " + spaceToPlayer); // para comprobar si se ha realizado correctamente

        if (spaceToPlayer < detectionRange && Mathf.Abs(spaceToPlayer) > 1)
        { // si la distancia del jugador es inferior a su rango de deteccion el enemigo perseguira al Player pero solo hasta que este cerca de este momento en el que atacara
            ChasePlayer();
            anim.SetBool("Walk", true); // si esta dentro del rango de deteccion pasa al estado Walk de animacion
            anim.SetBool("Attack", false); // no activaremos la animacion de atacar pues esta persiguiendo
        }
        else if (Mathf.Abs(spaceToPlayer) < 1)
        { // ya esta sobre el Player, entonces ataca
            anim.SetBool("Attack", true);
        }
        else
        { // si el Player esta lejor el enemigo se queda donde esta, vigilando
            StayVigilant();
            anim.SetBool("Walk", false);
            anim.SetBool("Attack", false);
        }


        DamageBoss(); //seguimiento en tiempo real de la barra de vida del Boss

        if(spaceToPlayer >= 4f) {
            castingSpell(); // lanza el hechizo
        }
    }

    public void ChasePlayer()
    {
        if (transform.position.x < player.position.x && !facingRight)
        { // Player se encuentra a la izquierda del enemigo y este se movera hacia la izquierda
            rb.velocity = new Vector2(velocity, 0f); // toma el valor de la velocidad del script de enemigo
            Flip(); // giramos al enemigo
        }
        else if (transform.position.x > player.position.x && facingRight)
        { // en caso contrario ira hacia la derecha
            rb.velocity = new Vector2(-velocity, 0f);
            Flip(); // giramos al enemigo
        }
        else if (!facingRight)
        {
            rb.velocity = new Vector2(-velocity, 0f);
        }
        else if (facingRight)
        {
            rb.velocity = new Vector2(velocity, 0f);
        }
    }

    public void StayVigilant()
    {
        rb.velocity = Vector2.zero; // quitamos la velocidad a 0
    }

    public void Flip()
    {
        facingRight = !facingRight; // si no camina hacia la derecha lo hara hacia la izquierda y viceversa  
        Vector3 newScale = transform.localScale;
        newScale.x *= -1;
        transform.localScale = newScale;
        //transform.localScale *= new Vector2(-1, transform.localScale.y); // multiplicamos por -1 la escala del eje x para que se de la vuelta
    }

    public void DamageBoss()
    {
        BossesUI.instance.MortalKombat(); // iniciaremos el Mortal Kombat
        remainingLife = GetComponent<Enemy>().healthPoints; // asociamos la vida del script Enemy a la vida del Boss
        BossHealthBar.fillAmount = remainingLife / bossHealth;
    }

    // Armagrog lanzara un hechizo de rango si la distancia respecto al jugador es mayor que 4
    public void castingSpell()
    {
        //GameObject spell = Instantiate(fireBall, transform.position, Quaternion.identity); // creamos una instancia desde la posicion donde aparezca el liche y atacamos sin que haya rotaion del hechizo
    }
}
