using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonScript : MonoBehaviour
{
    // patron singleton que gestionara todo los casos del GameManager
    public static SingletonScript instance;
    private void Awake() {
        instance = this;
    }

}
