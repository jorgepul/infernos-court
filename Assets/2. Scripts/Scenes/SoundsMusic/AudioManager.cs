using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio; // esta biblioteca gestionara los sonidos
using UnityEngine.UI; // esta biblioteca gestionara los sonidos relacionados con la UI

public class AudioManager : MonoBehaviour
{
    
    public AudioMixer effects; // mixer de los efectos sonoros
    public AudioMixer music; // mixer de la musica de fondo

    // declaracion de los efectos que van a ser usados
    public AudioSource gold; // sonido encargado de activarse cuando el Player consiga oro
    public AudioSource potion; // sonido encargado de activarse cuando el Player consiga una pocion
    public AudioSource levelUp; // sonido encargado de activarse cuando el Player suba de nivel
    public AudioSource enemyDeath; // sonido encargado de activarse cuando el Player mate a un enemigo
    public AudioSource enemyHitted; // sonido encargado de activarse cuando el Player hiera a un enemigo
    public AudioSource playerDeath; // sonido encargado de activarse cuando el Player muera
    public AudioSource playerHitted; // sonido encargado de activarse cuando el Player sea herido
    public AudioSource playerSword; // sonido encargado de activarse cuando el Player golpee con la espada
    public AudioSource ost; // sonido encargado de activarse en todo momento pues es la banda sonora de fondo
    public AudioSource mainMenu; // sonido encargado de activarse en todo momento pues es la banda sonora de fondo


    public float musicVolume; // volumen de la musica
    public float effectsVolume; // volumen de los efectos

    public Slider musicSlider; // slider para controlar el volumen de la ost desde el menu pausa
    public Slider effectsSlider; // slider para controlar el volumen de la ost desde el menu pausa


    [Range(-80,20)] // min y max en db que tendra nuestra musica de volumen
    // patron singleton para actualizar el audio a ejecutar
    public static AudioManager instance; // instancia de clase
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }


    public void audioToPlay(AudioSource audio) {
        audio.Play(); // se ejecutara la pista de audio correspondiente al efecto
    
    }

    
        


    // Start is called before the first frame update
    void Start() {
        audioToPlay(ost); // desde el inicio cargamos la musica de fondo que sera la ost
        //musicSlider.value = musicVolume;
        //effectsSlider.value = effectsVolume;

        // control de los maximos y minimos de los volumenes
        musicSlider.minValue = -80;
        musicSlider.maxValue = 10;
        effectsSlider.minValue = -80;
        effectsSlider.maxValue = 10;

        // asociaremos al valor de musicSlider el asignado por la clase saveSystem
        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 0f);
        effectsSlider.value = PlayerPrefs.GetFloat("EffectsVolume", 0f);

    }

    // Update is called once per frame
    void Update()
    {
        //MusicVolume();
        //EffectsVolume();
    }

    public void MusicVolume() {

        SaveSystem.instance.dataMusic(musicSlider.value); // guardaremos el valor de la musica como instancia 
        music.SetFloat("Music Volume", PlayerPrefs.GetFloat("MusicVolume")); // el mixer de audio tendra el valor del player preds de la clase saveSystem
        //music.SetFloat("Music Volume", musicSlider.value);
    }

    public void EffectsVolume() {
        SaveSystem.instance.dataEffects(effectsSlider.value); // guardaremos el valor de la musica como instancia 
        effects.SetFloat("Effects Volume", PlayerPrefs.GetFloat("EffectsVolume")); // el mixer de audio tendra el valor del player preds de la clase saveSystem
        //effects.SetFloat("Effects Volume", effectsSlider.value);
    }
}
