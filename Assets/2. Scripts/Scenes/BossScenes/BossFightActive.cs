using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossFightActive : MonoBehaviour
{
    // Cuando el player entre dentro de la zona del trigger se activaran los mecanismos para sellar el area de combate

    public GameObject bossActive; 

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            //BossesUI.instance.MortalKombat(); // iniciaremos el Mortal Kombat
            StartCoroutine (bossRevealing()); // llamamos al metodo de Aparicion del Boss
        }
    }

    IEnumerator bossRevealing () {

        var instanceSpeed = PlayerControler.instance.speed;
        PlayerControler.instance.speed = 0; //el Player no podra moverse hasta que aparezca el Boss, estilo Megaman
        bossActive.SetActive(true); // el boss aparecera
        yield return new WaitForSeconds(3f); // esperamos 3 segundos en los que no podra moverse el Player
        PlayerControler.instance.speed = instanceSpeed; // recuperamos el valor de speed y Player podra volverse a moverse
        Destroy(gameObject); // destruimos el trigger para evitar que se vuelva a ejecutar la espera de 3 sg
    }

    // Start is called before the first frame update
    void Start()
    {
        bossActive.SetActive(false); // al inicio el boss estara desactivado
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
