using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class BossesUI : MonoBehaviour
{

    public GameObject bossLifeBar; // indicara la interface para ver la vida de los jefes
    public GameObject fightArea; // indicara la interface que se generara cuando en una batalla final contra un jefe se acote la zona 

    public static BossesUI instance; // instancia de clase
    private void Awake() {
        if (instance == null) { 
            instance = this;
        }
    }



    // Start is called before the first frame update
    void Start() {
        bossLifeBar.SetActive(false); // al iniciar estaran inactivos para que no se vean en la pantalla
        fightArea.SetActive(false); //se activaran cuando el player acceda al area de combate del boss
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MortalKombat() {
        bossLifeBar.SetActive(true); // cuando se active el trigger de area se iniciara la pelea
        fightArea.SetActive(true); //se activaran cuando el player acceda al area de combate del boss
    }

    public void FinishHim() {
        bossLifeBar.SetActive(false); // cuando el Boss haya muerto se desactiva el trigger de area se iniciara la pelea
        fightArea.SetActive(false); //cuando el Boss haya muerto se desactivaran y el player podra salir del area de combate del boss
        StartCoroutine(bossDefeat());
    }

    
    IEnumerator bossDefeat() { //evitara que el Player se mueva hasta que el boss haya terminado su animacion de muerte

        var rbVelocity = PlayerControler.instance.GetComponent<Rigidbody2D>().velocity; // creamos la variable para mejorar la legibilidad del codigo

        Vector2 playerSpeed = rbVelocity; // cogemos el componente de la velocidad del rigidbody del Player
        rbVelocity = new Vector2(0, rbVelocity.y);
        PlayerControler.instance.enabled = false; // bloqueo el control
        yield return new WaitForSeconds(3); // espero 3 segundos
        PlayerControler.instance.enabled = true; // recuperamos el control
        rbVelocity = playerSpeed; // devolvemos el parametro de la velocidad del Player

    }


}
