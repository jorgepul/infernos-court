using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    /*La camara en este tipo de juegos se centra en el personaje*/

    public Transform player; //asignaremos al player como componente de la camara
    public Transform stage; // transform que delimitara la camara en la pantalla actual para que no se vea las "costuras"

    [Range(-5, 5)] // movera el valor entre dos rangos 
    public float xMinCameraModifier; // modificador de la distancia minima en x
    public float xMaxCameraModifier; // modificador de la distancia maxima en x
    public float yMinCameraModifier; // modificador de la distancia minima en y
    public float yMaxCameraModifier; // modificador de la distancia maxima en y

    public float cameraSpeed; // variable que rdeterminara la velocidad de la camara para darle un aspecto de que el jugador es mas rapido


    // patron singleton para la camara
    public static CameraController instance; // instancia de clase
    private void Awake() {
        if (instance == null) {
            instance = this;
        } 
    }

    // Start is called before the first frame update
    void Start()
    {   // Al iniciar el juego le indicamos a la camara que tiene que ir donde el jugador
        //player = PlayerControler.instance.gameObject.transform;
        stage = player;
        transform.position = new Vector3(player.position.x, player.position.y, -1);
    }

    // Update is called once per frame
    void Update() {

        // determinar los limites de la camara
        var minYPosition = stage.GetComponent<BoxCollider2D>().bounds.min.y + yMinCameraModifier; // posicion minima a la que llegara la camara en el eje Y segun las coordenadas del box collider que le hemos puesto en unity manualmente
        var maxYPosition = stage.GetComponent<BoxCollider2D>().bounds.max.y + yMaxCameraModifier; // maxima Y
        var minXPosition = stage.GetComponent<BoxCollider2D>().bounds.min.x + xMinCameraModifier; // minima X
        var maxXPosition = stage.GetComponent<BoxCollider2D>().bounds.max.x + xMaxCameraModifier; // maxima X

        //anclara la camara segun unas referencias dadas: el jugador y el fondo de camara
        Vector3 positions = new Vector3(
            Mathf.Clamp(player.position.x, minXPosition, maxXPosition), 
            Mathf.Clamp(player.position.y, minYPosition, maxYPosition),
            Mathf.Clamp(player.position.z, -10f, -10f));

        // el uso de la funcion lerp() tendra en cosideracion el tiempo par moverse entre dos vectores dados, uno sera la posicion inicial de la camara, la posicion de esta y la velocidad a la que queremos de vaya
        Vector3 cameraSlowMo = Vector3.Lerp(transform.position, positions, cameraSpeed* Time.deltaTime);

        // tomara la posicion del player en cada frame pero el componente Z lo asignamos manualmente para no coja el del player y asi no muestre las otras capas de las pantallas
        transform.position = cameraSlowMo;
    }
}
