using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision) {
        //cuando el jugador entra se tomara el objeto child de la pantalla se activara         
        if (collision.CompareTag("Player")) {
            transform.GetChild(0).gameObject.SetActive(true);
            CameraController.instance.stage = transform.GetChild(0);
        }
    }

    private void OnTriggerStay2D(Collider2D collision) {
        // mientras el Player se encuentre dentro del trigger de ese stage child seguiremos en ese stage child
        if (collision.CompareTag("Player")) {
            CameraController.instance.stage = transform.GetChild(0);
        }
        
    }


    private void OnTriggerExit2D(Collider2D collision) {
        // al salir del trigger y es el player, desactivamos el stage child
        if (collision.CompareTag("Player")) {
            transform.GetChild(0).gameObject.SetActive(false);
        }
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
