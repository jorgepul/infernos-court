using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoxShop : MonoBehaviour
{
    public GameObject[] itemsInShop; // array con los elementos de la tienda
    InventaryScript inventory;

    public Animator anim; // al hablar con el comerciante activar la casilla de compra

    public bool sellingItems;

    // Start is called before the first frame update
    void Start()
    {
        inventory = gameObject.GetComponent<InventaryScript>();
        SetUpShop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    // si se desarrolla diferentes comerciantes asi podriamos a�adir a cada uno de ellos un tipo de elemento a vender: comida, armas...
    // pero para esta version no hara falta
    private void SetUpShop() {

        for (int i = 0; i < itemsInShop.Length; i++) { // buscara en todos los slots del inventario e instanciamos los items de la tienda

            if (itemsInShop[i]==null) { // si el comerciante no tiene nada que vender no podremos acceder a la tienda
                break;
            }

            GameObject itemsToSell = Instantiate(itemsInShop[i], inventory.slots[i].transform.position, Quaternion.identity);
            itemsToSell.transform.SetParent(inventory.slots[i].transform, false);
            itemsToSell.transform.localPosition = new Vector3(0, 0, 0);
            itemsToSell.name = itemsToSell.name.Replace("(Clone)","");
        }
    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        anim.SetBool("ShowShop", true); // aparece la pantalla de la tienda
        SingletonScript.instance.gameObject.GetComponent<PauseSystem>().pauseGame(); // pausamos el juego al entrar en la tienda
    }

    public void ExitShop () { // al salir de la tienda
        anim.SetBool("ShowShop", false); // desaparece la pantalla de la tienda
        StartCoroutine(DisableShop());
        SingletonScript.instance.gameObject.GetComponent<PauseSystem>().pauseGame();
    }

    IEnumerator DisableShop() { // para evitar que se active la tienda por estar cerca del NPC al cerrar la tienda
        gameObject.GetComponent<BoxCollider2D>().enabled = false; // desactivamos la tienda
        yield return new WaitForSeconds(3f); // esperamos 3 sg 
        gameObject.GetComponent<BoxCollider2D>().enabled = true; // activamos la tienda
    }

    public void IsSellingItems() {
        sellingItems = !sellingItems;
    }
}
