using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopSystem : MonoBehaviour
{
    public string itemName; // nombre del item de la tienda
    public int itemSellPrice; // precio del item en la tienda al venderlo 
    public int itemBuyPrice; // precio del item en la tienda al compralo 
    TextMeshProUGUI itemBuyPriceText;

    public GameObject itemToAdd; // item de pociones que se va a vender en la tienda.
    public int numberOfItemsToAdd; // numero de items que se incorporaran a la tienda
    InventaryScript inventory;
    SingletonScript gameManager;

    public FoxShop shopNPC;



    // Start is called before the first frame update
    private void Start()
    {
        gameManager = SingletonScript.instance;
        inventory = gameManager.GetComponent<InventaryScript>();
        itemName = itemToAdd.name;
        itemBuyPriceText = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        itemBuyPriceText.text = "$ " + itemBuyPrice.ToString(); // mostrara el signo deldolar para "indicar" la divisa
        shopNPC = transform.root.GetComponent<FoxShop>(); // buscara el primer transform
    }


    public void BuyItem()
    {

        if (!shopNPC.sellingItems)
        { // si no estoy vendiendo items es que los estoy comprando
            if (itemBuyPrice <= totalMoney.instance.moneyInTotal)
            {
                totalMoney.instance.money(-itemBuyPrice); // resto el precio de venta del total que tenemos acumulado
                inventory.CheckEmptySlots(itemToAdd, itemToAdd.name, numberOfItemsToAdd);
            }
            else
            {
                // error sound
            }

        }
        else if (inventory.inventoryItems.ContainsKey(itemToAdd.name))
        {
            itemBuyPriceText.text = "$ " + itemSellPrice.ToString();
            inventory.UseItemFromInventary(itemToAdd.name);
            totalMoney.instance.money(itemSellPrice); // a�adimos el precio de venta a nuestra cuenta
        }
        else
        {
            // error sound
        }
    }

    public void UpdateText()
    {
        if (!shopNPC.sellingItems)
        {
            itemBuyPriceText.text = "$ " + itemBuyPrice.ToString();
        }
        else
        {
            itemBuyPriceText.text = "$ " + itemSellPrice.ToString();
        }


    }

    // Update is called once per frame
    void Update()
    {
        UpdateText(); // actualizamos el contador de dinero
    }
}
