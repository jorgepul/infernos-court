using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopItems : MonoBehaviour
{

    public string itemName; // nombre del item de la tienda
    public int itemSellPrice; // precio del item en la tienda al venderlo 
    public int itemBuyPrice; // precio del item en la tienda al compralo 
    TextMeshProUGUI itemBuyPriceText;

    public GameObject itemToAdd; // item de pociones que se va a vender en la tienda.
    public int numberOfItemsToAdd; // numero de items que se incorporaran a la tienda
    InventaryScript inventory;
    SingletonScript gameManager;



    // Start is called before the first frame update
    private void Start()
    {
        gameManager = SingletonScript.instance;
        inventory = gameManager.GetComponent<InventaryScript>();
        itemName = itemToAdd.name;
        itemBuyPriceText = gameObject.GetComponentInChildren<TextMeshProUGUI>();
        itemBuyPriceText.text = "$ " + itemBuyPrice.ToString(); // mostrara el signo deldolar para "indicar" la divisa
    }


    public void BuyItem()
    {
        if (itemBuyPrice <= totalMoney.instance.moneyInTotal)
        {
            totalMoney.instance.money(-itemBuyPrice); // resto el precio de venta del total que tenemos acumulado
            inventory.CheckEmptySlots(itemToAdd, itemToAdd.name, numberOfItemsToAdd);
        }
    }

}
