using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxControl : MonoBehaviour
{


    public Animator animOptions; // para llamar a la animacion de options
    public Animator animShop; // para llamar a la animacion de shop

    public void EnterOptions()
    { // funcion que hara aparecer el menu de settings
        animOptions.SetBool("ShowOptions", false);
    }

    public void HideOptions()
    { // funcion que pcultara el menu de settings
        animOptions.SetBool("ShowOptions", true);
    }

    public void EnterShop()
    { // funcion que hara aparecer el menu de shop
        animShop.SetBool("ShowShop", true);
    }

    public void HideShop()
    { // funcion que pcultara el menu de shop
        animShop.SetBool("ShowShop", false);
    }
}
