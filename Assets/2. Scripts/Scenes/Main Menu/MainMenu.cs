using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // dado que con el menu principal tenemos 2 escenas sera necesario para gestionarlas en el correcto orden

public class MainMenu : MonoBehaviour
{
    public Animator animSettings; // para llamar a la animacion de settings
    public Animator animControls; // para llamar a la animacion de settings

    // Start is called before the first frame update
    void Start() {
        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "MainMenu") {
            AudioManager.instance.ost.Stop(); // paramos la cancion de juego
            AudioManager.instance.audioToPlay(AudioManager.instance.mainMenu); // ponemos la melodia asociada al menu principal
        }

        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame() {
        
        SceneManager.LoadScene(1); // al presionar en el boton asociado del inspector cargara la scena 1 que corresponde al juego
    }

    public void ExitGame() {
        Application.Quit(); // quitaremos el juego
        print("Game Closed");
    }

    public void ReturnMainMenu () {
        SceneManager.LoadScene(0); // al presionar en el boton asociado del inspector cargara la scena 0 que corresponde al menu principal
    }

    public void EnterSettings() { // funcion que hara aparecer el menu de settings
        animSettings.SetBool("ShowMenu", true);
    }

    public void HideSettings() { // funcion que pcultara el menu de settings
        animSettings.SetBool("ShowMenu", false);
    }

    
    public void EnterControls()
    { // funcion que hara aparecer el menu de settings
        animControls.SetBool("ShowControls", true);
    }

    public void HideControls()
    { // funcion que pcultara el menu de settings
        animControls.SetBool("ShowControls", false);
    }
    
}
