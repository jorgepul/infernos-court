using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventaryAppScript : MonoBehaviour
{

    InventaryScript inventory;
    SingletonScript gameManager;


    // Start is called before the first frame update
    private void Start()
    {
        gameManager = SingletonScript.instance;
        inventory = gameManager.GetComponent<InventaryScript>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UsingItemsFromInventory() {
        inventory.UseItemFromInventary(gameObject.name);
    
    }
}
