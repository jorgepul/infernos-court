using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InventaryScript : MonoBehaviour
{

    public GameObject[] slots; // array que contendra las coordenadas del grid del inventario
    public GameObject[] backPack; // 

    private bool isInstanciate; // determinara si el objeto del inventario ha sido instanciado

    public Dictionary<string, int> inventoryItems = new Dictionary<string, int>() ; // diccionario para el nombre del item del inventario y su cantidad

    TextMeshProUGUI text; // recoge el valor de la cuenta de los items de un tipo en el inventario


    // metodo que permite determinar si hay o no slots disponibles en el inventario para guardar cosas en el.
    public void CheckEmptySlots(GameObject itemToAdd, string itemName, int itemNumber) {
        isInstanciate = false; // falso porque no hay ningun item en el inventario
        for (int i = 0; i < slots.Length ; i++) { // bucle for para recorrer uno a uno el array de slots
            if (slots[i].transform.childCount > 0) {
                // si el slot esta ocupado no haremos nada
                slots[i].GetComponent<SlotsScript>().isTaken = true; // determinamos que el slot esta ocupado
            } 
            else if (!isInstanciate && !slots[i].GetComponent<SlotsScript>().isTaken) {
                // si el slot esta vacio, crearemos el item en ese slot
                if (!inventoryItems.ContainsKey(itemName)) { // si el diccionario no contiene
                    
                    GameObject item = Instantiate(itemToAdd, slots[i].transform.position, Quaternion.identity); // creamos el item en esa posion del slot
                    item.transform.SetParent(slots[i].transform, false); // asignamos al item el slot que estamos revisando
                    item.transform.localPosition = new Vector3(0, 0, 0);
                    item.name = item.name.Replace("(Clone)", ""); // sustituimos el nombre por un espacio en blanco
                    isInstanciate = true;
                    slots[i].GetComponent<SlotsScript>().isTaken = true; // determinamos que el slot esta ocupado al crearse el item en el
                    inventoryItems.Add(itemName, itemNumber);
                    text = slots[i].GetComponentInChildren<TextMeshProUGUI>();
                    text.text = itemNumber.ToString(); // pasamos el numero de la cantidad de objetos recogidos a un string
                    break; // rompo el bucle for

                } else { // en caso de si tener el item en el inventario y recogemos otro
                    
                    for (int j = 0; i < slots.Length; j++) { // recorremos el inventario buscandolo
                        // si el hijo del transform tiene el mismonombre del item que acabamos de recoger entonces ya la tendriamos y aumentariamos su numero de inventario
                        if (slots[j].transform.GetChild(0).gameObject.name == itemName) {
                            inventoryItems[itemName] += itemNumber;
                            text = slots[j].GetComponentInChildren<TextMeshProUGUI>();
                            text.text = inventoryItems[itemName].ToString();
                            break;
                        }             
                    }
                    break; // rompo el bucle for


                }
            
            }
        }
    }

    // metodo que nos permitira ejecutar la mecanica correspondiente al seleccionar el item en el inventario
    public void UseItemFromInventary(string itemName) {
        for (int i = 0; i < slots.Length; i++) {
            if (slots[i].transform.GetChild(0).gameObject.name == itemName ) {
                text = slots[i].GetComponentInChildren<TextMeshProUGUI>();
                inventoryItems[itemName] --; // resto el item del inventario al usarlo
                text.text = inventoryItems[itemName].ToString();

                if (inventoryItems[itemName] <= 0) {
                    Destroy(slots[i].transform.GetChild(0).gameObject) ; // en caso de quedarnos sin ese items, lo "destruimos" del inventario deníjando el slot libre
                    slots[i].GetComponent<SlotsScript>().isTaken = false;
                    inventoryItems.Remove(itemName); // quitamos el nombre del diccionario
                    ArrangeInventory(); // al gastar un item reorganizamos el inventario
                }
                break;
            }
        
        }



    }

    // en caso de gastar un consumible, reorganizamos el inventario para que ocupen siempre los primeros slots disponibles
    private void ArrangeInventory() {
        for (int i = 0; i < slots.Length; i++) {
            if (!slots[i].GetComponent<SlotsScript>().isTaken) { // sino esta usado el slot voy a mover el siguiente
                for (int j = i + 1; i < slots.Length; j++)  {
                    if (slots[j].GetComponent<SlotsScript>().isTaken)  {
                        Transform itemToMove = slots[j].transform.GetChild(0).transform;
                        itemToMove.transform.SetParent(slots[i].transform, false);
                        itemToMove.transform.localPosition = new Vector3(0, 0, 0);
                        slots[i].GetComponent<SlotsScript>().isTaken = true; // ocupamos asi el slot que antes estaba vacio
                        slots[j].GetComponent<SlotsScript>().isTaken = false; // decimos que al mover el item el slot de partida vuelve a estar libre
                        break;
                    }

                }

            }
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
