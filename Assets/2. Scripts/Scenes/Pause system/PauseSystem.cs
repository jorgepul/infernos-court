using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseSystem : MonoBehaviour
{
    public GameObject pauseMenu;
    public bool isPaused; // determinara si nos encontramos con el menu de pausa activo


    private void Awake() {
        Time.timeScale = 1;
        pauseMenu.SetActive(false); // se desactiva el menu de pausa
        isPaused = false; // determinamos que el juego NO esta pausado
    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        pauseGame(); // llamamos al menu de pausa
    }

    public void pauseGame() {
        if (Input.GetKeyDown(KeyCode.Escape) && !isPaused) { // al presionar la tecla "Esc" aparecera el menu y no esta pausado el juego
            Time.timeScale = 0;
            pauseMenu.SetActive(true); // se activa el menu de pausa
            isPaused = true; // determinamos que el juego esta pausado
        } else if (Input.GetKeyDown(KeyCode.Escape) && isPaused) { // al volver a pulsar "Esc" volvemos al juego
            Time.timeScale = 1;
            pauseMenu.SetActive(false); // se desactiva el menu de pausa
            isPaused = false; // determinamos que el juego NO esta pausado
        }
    
    }

}
