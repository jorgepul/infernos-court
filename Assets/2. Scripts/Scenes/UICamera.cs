using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICamera : MonoBehaviour
{
    // camara que controla la imagen del personaje en cada momento

    public Transform player;
    // posiciones de los 3 vectores (cada eje) que controlan la camara
    public float xpos;
    public float ypos;
    public float zpos;


    // Start is called before the first frame update
    void Start()
    {  
        transform.position = new Vector3(player.position.x + xpos, player.position.y + ypos, zpos);
    }

    // Update is called once per frame
    void Update()
    {
        // cada vez que se mueva la camara estara delante del Player
        transform.position = new Vector3(player.position.x + xpos, player.position.y + ypos, zpos);
    }
}
