using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MonoBehaviour
{

    // patron singleton para actualizar sistema de guardado
    public static SaveSystem instance; // instancia de clase
    private void Awake() {
        if (instance == null)  {
            instance = this;
        }
        else if (instance != null) { // en caso de haber varios save system se destruira el que se crea
            Destroy(gameObject);
        }
    }


    // Start is called before the first frame update
    void Start() {
        DontDestroyOnLoad(gameObject); // evitamos asi que el save game se destruya y que por tanto sea persistente
    }

    public void dataMusic(float value) {
        PlayerPrefs.SetFloat("MusicVolume", value);  // guardara en el sistema el volumen que el jugador haya puesto de una partida

    }

    public void dataEffects(float value) {
        PlayerPrefs.SetFloat("EffectsVolume", value);  // guardara en el sistema el volumen de los efectos que el jugador haya puesto de una partida
    }

    public void ExperienceSavingData( float value) { // esta funcion guardara le experiencia del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("currentExp", value);
    }

    public void NextLevelExperienceSavingData(float value) { // esta funcion guardara le experiencia del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("nextLevelEXP", value);
    }

    public void LevelSavingData(int value) { // esta funcion guardara el nivel del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetInt("level", value);
    }

    public void HealthSavingData(float value) { // esta funcion guardara la salud maxima del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("maxHealth", value);
    }

    public void MagicSavingData(float value)
    { // esta funcion guardara la salud maxima del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("maxMana", value);
    }


    public void STRSavingData(float value)  { // esta funcion guardara la fuerza del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("STR", value);
    }

    public void DEFSavingData(float value)  { // esta funcion guardara la defensa del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("DEF", value);
    }

    public void ATTCKSavingData(float value) { // esta funcion guardara el ataque del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("ATTCK", value);
    }

    public void GOLDSavingData(float value) { // esta funcion guardara el dinero del personaje asi al restaurar desde un punto de guardado no empezara en nivel 1
        PlayerPrefs.SetFloat("Gold", value);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
