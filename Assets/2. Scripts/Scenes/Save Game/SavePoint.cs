using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePoint : MonoBehaviour
{

    // al tocar el icono se guardara la partida
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {

            ExperienceSystem.instance.SaveGame(); // guardamos la partida

            print("Game saved");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
