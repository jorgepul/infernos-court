using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAchieve : MonoBehaviour
{
    public GameObject gameAchieveImage; // cargaremos la pantalla de fin de juego completo
    public bool isTaken;

    // Start is called before the first frame update
    void Start()
    {
        gameAchieveImage.SetActive(false); // ocultamos la pantalla de game over
        isTaken = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            CompleteGame();
        }            
    }


    public void CompleteGame() {
        if ((gameObject.name == "Godness Light") && isTaken==false) {
            Time.timeScale = 0; // se congelara el juego
            gameAchieveImage.SetActive(true); // ocultamos la pantalla de game over
            AudioManager.instance.playerSword.Stop(); // paramos el sonido de la espada que se detecto en la prueba de usuarios
            AudioManager.instance.ost.Stop(); // Pararemos la musica
        }


    }

}
