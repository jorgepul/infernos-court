using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WeaponScript : MonoBehaviour
{

    float attack;
    float totalAttack;
    public float weaponAttack; // valor que mide el ataque del arma equipada

    // patron singleton para actualizar los atributos del armma
    public static WeaponScript instance; // instancia de clase
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public GameObject damageText; // mostrara el da�o en la pantalla cuando el enemigo lo reciva

    // Start is called before the first frame update
    void Start() {
        attack = PlayerControler.instance.attackWeapon;
    }

    // Update is called once per frame
    void Update()  {
        
    }

    public float DamageInput(float enemyDefense) { 
    totalAttack = attack + weaponAttack + (100/(100+enemyDefense));
        float finalAttackPower = Mathf.Round(Random.Range(totalAttack - 10, totalAttack + 10));
        if (finalAttackPower > totalAttack+4) {
            finalAttackPower *= 2;
            print("Critical");
        }

        if (finalAttackPower < 0) {
            finalAttackPower = 0;
            print("Block");
        }

        GameObject textGo = Instantiate(damageText, transform.position, Quaternion.identity);
        textGo.GetComponent<TextMeshPro>().SetText(finalAttackPower.ToString());

        print(finalAttackPower);
        return finalAttackPower;
    
    }

    
}
