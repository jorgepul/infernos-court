using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; // biblioteca para poder interaccionar con imagenes pej la barra de vida del Player

public class PlayerHealth : MonoBehaviour
{
    public float health; // salud actual del personaje
    public float maxHealth; // maxima salud del personaje 
    bool isInmune; 
    public float inmunityTime; // variable que me permite tener una cierta inmortalidad tras ser golpeado
    SpriteRenderer sprite;
    Blink material;
    Rigidbody2D rb;
    public float knockBackForceX; // medira el retroceso en el eje X del personaje al ser golpeado
    public float knockBackForceY; // medira el retroceso en el eje Y del personaje al ser golpeado
    public Image healthImage;
    //Animator anim;
    public GameObject deathEffect; // cargaremos la anmacion de muerte al quedarnos sin vida
    public bool isPlayerDead; // determinara si el player esta muerto
    public GameObject gameOverImage; // cargaremos la pantalla de game over

    public float mana; // salud actual del personaje
    public float maxMana; // maxima salud del personaje 
    public Image manaImage;

    // patron singleton para actualizar la salud al subir de nivel
    public static PlayerHealth instance; // instancia de clase
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        gameOverImage.SetActive(false); // ocultamos la pantalla de game over
        
        sprite = GetComponent<SpriteRenderer>();
        material = GetComponent<Blink>();
        rb = GetComponent<Rigidbody2D>();
        //anim = GetComponent<Animator>();
        /*if (!isPlayerDead) {
            gameOverImage.GetComponent<CanvasGroup>().alpha = 0.0f;
        }*/
        maxHealth = PlayerPrefs.GetFloat("maxHealth", maxHealth);
        health = maxHealth; // al iniciar el juego el personaje tiene su salud completa

        maxMana = PlayerPrefs.GetFloat("maxMana", maxMana);
        mana = maxMana; // al iniciar el juego el personaje tiene su barra de magia al completo
    }

    // Update is called once per frame
    void Update()
    {
        if (health > maxHealth) // si recuperamos mas vida que la maxima del personaje nos quedaremos con la maxima, no superamos la maxima en caso de curarnos en exceso
        {
            health = maxHealth;
        }

        healthImage.fillAmount = health / maxHealth;


        if (mana > maxMana) // si recuperamos mas magia que la maxima del personaje nos quedaremos con la maxima, no superamos la cantidad maxima maxima 
        {
            mana = maxMana;
        }

        manaImage.fillAmount = mana / maxMana;


        DeadPlayer(); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.CompareTag("Enemy")  && !isInmune) {
            health -= (2* (collision.GetComponent<Enemy>().attack * collision.GetComponent<Enemy>().strenght) - GetComponent<PlayerControler>().defense); // si el personaje entra en contacto con un enemigo se le restara el valor de ataque de este a su salud
            AudioManager.instance.audioToPlay(AudioManager.instance.playerHitted); // se ejecutara el sonido correspondiente al interaccionar
            //health -= collision.GetComponent<Enemy>().attack; // si el personaje entra en contacto con un enemigo se le restara el valor de ataque de este a su salud

            StartCoroutine(Inmunity()); // al ser golpeado ejecuta la animacion

            // para evitar que el jugador al ser golpeado se desplace hacia atras siempre en el eje X positivo
            // de modo que si el enemigo esta a la izquierda el jugador se ira hacia la derecha y viceversa
            if (collision.transform.position.x < transform.position.x)
            {
                rb.AddForce(new Vector2(knockBackForceX, knockBackForceY), ForceMode2D.Force); // hacia la derecha
            }
            else
            {
                rb.AddForce(new Vector2(-knockBackForceX, knockBackForceY), ForceMode2D.Force); // hacia la izquierda
            }


            if (health <= 0) { // cuando la salud sea menor que cero el personaje habra muerto
                AudioManager.instance.audioToPlay(AudioManager.instance.playerDeath); // se ejecutara el sonido correspondiente al interaccionar            
                isPlayerDead = true;
                //print("player dead"); // mostraremos la pantalla de game over
                //Instantiate(deathEffect, transform.position, Quaternion.identity); // esto permite a la animacion de EnemyDeath activarse cuando la salud llegue a 0
                //Destroy(gameObject); // el player sera destruido con la animacion de muerte
                //anim.SetBool("Death", true);  //si el personaje esta en el aire se inicia la animacion de Jump
                
            }        
        }
    }

    // metodo para cercionarnos de que unicamente el enemigo haga da�o mas de una vez al entrar en contacto con el personaje 
    IEnumerator Inmunity()
    {
        isInmune = true;
        sprite.material = material.blink; // al ser golpeado cambia la "animacion" por la de golpeado
        yield return new WaitForSeconds(inmunityTime);
        isInmune = false;
        sprite.material = material.original; // pasados los 0.5f segundos volvera al sprite original
    }

    public void DeadPlayer() {

        if (isPlayerDead == true) { // si el player esta muerto
            Time.timeScale = 0; // se congelara el juego
            gameOverImage.SetActive(true); // ocultamos la pantalla de game over
            AudioManager.instance.playerSword.Stop(); // paramos el sonido de la espada que se detecto en la prueba de usuarios
            AudioManager.instance.ost.Stop(); // Pararemos la musica

            /*
            if (gameOverImage.GetComponent<CanvasGroup>().alpha < 1f)
            {
                gameOverImage.GetComponent<CanvasGroup>().alpha += 0.005f;
            }
            */
        }
        

    }
}
