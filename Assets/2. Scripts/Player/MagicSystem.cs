using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicSystem : MonoBehaviour
{

    public int neededMana; // variable que indica el numero de corazones que cuesta usar la arma secundaria
    public GameObject spell; // variable que determina al objeto sub weapon, en este caso un hacha


    // Update is called once per frame
    void Update()
    {
        UseMagic();
    }

    public void UseMagic()
    {

        // para diferencia el uso de las armas secundarias o de las magias y poder usar el mismo boton de accion
        // haremos que el arma secundaria solo sea disbarada al pulsar direccion hacia arriba + Fire2
        if (Input.GetButtonDown("Fire2") && neededMana <= PlayerHealth.instance.mana) { // si el usuario presiona el boton derecho del raton o alt y tiene el mana necesario (el coste sea inferior al total que tiene en ese momento el jugador) podra disparar el arma secundaria
                //Hearts.instance.subItem(-neededMana); // restaremos los corazones a la instancia de la clase hearts que determina el total de corazones en ese momento

                GameObject magicCasting = Instantiate(spell, transform.position, Quaternion.identity);

                PlayerHealth.instance.mana -= neededMana; // restamos el mana consumido para generar la magia

            if (transform.localScale.x < 0) { // la magia ira hacia la izquierda
                magicCasting.GetComponent<Rigidbody2D>().AddForce(new Vector2(-60f, 0f), ForceMode2D.Force); // determina la fuerza y direccion en que se dispara la lanza
                
            }
            else { // la magia ira hacia la derecha
                magicCasting.GetComponent<Rigidbody2D>().AddForce(new Vector2(60f, 0f), ForceMode2D.Force);
                magicCasting.transform.localScale = new Vector2(-1, 1); // giramos el hacha para que vaya en la correcta direccion
            }


            /*
             if (transform.localScale.x < 0)
                { // si el jugador esta mirando a la izquierda el hacha ira a la izquierda
                    subItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500f, 0f), ForceMode2D.Force);
                    subItem.transform.localScale = new Vector2(-1, -1); // giramos el hacha para que vaya en la correcta direccion
                }
                else
                { // si mira a la derecha el hacha ira en esa direccion
                    subItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(500f, 0f), ForceMode2D.Force);
                }
             */

        }


    }




    // Start is called before the first frame update
    void Start()
    {
        
    }


}
