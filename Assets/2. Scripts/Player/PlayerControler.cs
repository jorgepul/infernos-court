using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour
{
    public float strenght, attackWeapon, defense;
    //float power = attackWeapon* strenght;

    public float speed, jumpHeight;
    float velX;
    float velY;
    Rigidbody2D rb;
    public Transform groundcheck;
    public bool isGrounded;
    public float groundCheckRadious;
    public LayerMask whatIsGround;
    Animator anim;

    // patron singleton para actualizar la salud al subir de nivel
    public static PlayerControler instance; // instancia de clase
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        strenght = PlayerPrefs.GetFloat("STR", strenght);
        defense = PlayerPrefs.GetFloat("DEF", defense);
        attackWeapon = PlayerPrefs.GetFloat("ATTCK", attackWeapon);
    }

    // Update is called once per frame
    void Update() // funcion dependiente de la velocidad de la CPU
    {

        FlipCharacter(); // volteara al jugador en caso de que vaya hacia la izquierda
        isGrounded = Physics2D.OverlapCircle(groundcheck.position, groundCheckRadious, whatIsGround); // detectara si el personaje esta sobre una plataforma que se considera suelo
        if (isGrounded)
        {
            anim.SetBool("Jump", false);  //si el personaje esta en el suelo no se inicia la animacion de Jump
        }
        else
        {
            anim.SetBool("Jump", true);  //si el personaje esta en el aire se inicia la animacion de Jump
        }

        Attack();
        Block();
    }

    private void FixedUpdate()
    {
        Movement();
        Jump();
    }

    public void FlipCharacter()
    {
        if (rb.velocity.x > 1)
        {
            transform.localScale = new Vector3(1, 1, 1); // si es mayor de cero el personaje mira a la derecha
        }
        else if (rb.velocity.x < -1) // de esta manera cuando el personaje se quede quieto no mirara a la izquierda continuamente
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }

    public void Movement()
    {
        velX = Input.GetAxisRaw("Horizontal");
        velY = rb.velocity.y;
        rb.velocity = new Vector2(velX * speed, velY);

        if (rb.velocity.x != 0) {
            anim.SetBool("Walk", true);
        }
        else {
            anim.SetBool("Walk", false);
        }
       

    }

    public void Jump()
    {
        if (Input.GetButton("Jump") && isGrounded) {
            rb.velocity = new Vector2(rb.velocity.x, jumpHeight);
        }
    }

    public void Attack()
    {
        if (Input.GetButtonDown("Fire1")) //Fire1 es la tecla preprogramada para el ataque ver edit->projectSetting->input para ver el listado de botones
        {
            anim.SetBool("Attack", true);  // al presionar el mouse izq o control izq el personaje atacara
            AudioManager.instance.audioToPlay(AudioManager.instance.playerSword); // se ejecutara el sonido correspondiente al interaccionar
        }
        else
        {
            anim.SetBool("Attack", false);  // mientras no presione el boton de ataque el personaje no realizara la animacion Attack
        }
    }

    public void Block()
    {
        if (Input.GetButtonDown("Fire2")) //Fire2 es la tecla preprogramada para  ver edit->projectSetting->input para ver el listado de botones
        {
            anim.SetBool("Block", true);  // al presionar el mouse derecho o alt izq el personaje atacara
        }
        else
        {
            anim.SetBool("Block", false);  // mientras no presione el boton de defensa el personaje no realizara la animacion Block
        }
    }

}
