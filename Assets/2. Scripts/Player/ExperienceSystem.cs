using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExperienceSystem : MonoBehaviour
{

    public Image imageExperience;
    public Text levelText; // texto que mostrara en la UI el nivel actural del player
    public float currentExp; // experiencia actual del player que se indica en la UI
    public float nextLevelExp; // experiencia necesaria para subir de nivel
    public int level; // indica el nivel en el que se encuentra el player

    public GameObject LevelUpText; // mostrara el texto que indica que ha subido de nivel

    // patron singleton para la la experiencia
    public static ExperienceSystem instance; // instancia de clase
    private void Awake() {
        if (instance == null) {
            instance = this;
        }
    }


    // Start is called before the first frame update
    void Start(){
        //level = 1; // el nivel de partida es 1
        currentExp = PlayerPrefs.GetFloat("currentExp", 0f);
        nextLevelExp = PlayerPrefs.GetFloat("nextLevelEXP", nextLevelExp);
        level = PlayerPrefs.GetInt("level",1);
        levelText.text = level.ToString(); // convertimos el numero del nivel en un texto para mostrarlo por pantalla
        imageExperience.fillAmount = currentExp / nextLevelExp;

    }

    // Update is called once per frame
    void Update(){
        // si ponemos el relleno en el start necesitariamos de otro ataque para que acptara los cambios al subir de nivel
        // asi si al subir de nivel sobra exp esta se a�ade a la barra
        //imageExperience.fillAmount = currentExp / nextLevelExp; // la barra de experiencia se ira rellenando y cuando este llena el player subira de nivel

    }



    public void ExpCalculator(float exp) {

        currentExp += exp; //aumentaremos la experiencia actual con la que se recibe del enemigo

        // la barra de exp se modificara cada vez que se reciba experiencia
        imageExperience.fillAmount = currentExp / nextLevelExp; // la barra de experiencia se ira rellenando y cuando este llena el player subira de nivel
        levelText.text = level.ToString();


        if (currentExp >= nextLevelExp){ // si la experiencia actual es igual a la necesaria para subir de nivel
            
            level++; // aumentamos en 1 el nivel
            levelText.text = level.ToString();

            currentExp = currentExp - nextLevelExp; // reiniciamos el contador a la diferencia de los puntos actuales menos los necesarios para subir de nivel
            //currentExp = 0; // reiniciamos el contador a la diferencia de los puntos actuales menos los necesarios para subir de nivel
            nextLevelExp = level * (level + 1) * ((3 * level) + 8); // aumentaremos la experiencia necesaria segun la formula indicada
            //currentExp = nextLevelExp - currentExp; // reiniciamos el contador a la diferencia de los puntos actuales menos los necesarios para subir de nivel
            
            GameObject textGo = Instantiate(LevelUpText, transform.position, Quaternion.identity); // aparecera el tecto sobre el personaje
            StartCoroutine(textAbove(textGo)); // corrutina para desplazar el texto sobre el enemigo

            AudioManager.instance.audioToPlay(AudioManager.instance.levelUp); // se ejecutara el sonido correspondiente al interaccionar

            // cambio de parametros al subir de nivel
            // vida
            PlayerHealth.instance.maxHealth += 15f; // al subir de nivel aumentamos en 15 puntos de vida   
            // magia
            PlayerHealth.instance.maxMana += 10f; // al subir de nivel aumentamos en 10 puntos de vida    
            //Hearts.instance.maxHearts += 5; // al subir de nivel aumentamos en 5 la capacidad de los corazones
            // fuerza
            PlayerControler.instance.strenght += 1f; // al subir de nivel aumentamos en 1 la fuerza           
            // defensa
            PlayerControler.instance.defense += 1f; // al subir de nivel aumentamos en 1 la defensa            
            // ataque
            PlayerControler.instance.attackWeapon += 1f; // al subir de nivel aumentamos en 1 la fuerza del arma

            //levelText.text = level.ToString();
            //imageExperience.fillAmount = currentExp / nextLevelExp;
            // por ejemplo si pasar de nivel 1 a 2 requiere 22 de exp, para subir al nivel 3 requerira 84, y sucesivamente...
            //currentExp = 0; // reiniciamos el contador a 0 para que cueste mas subir de nivel
            //print("Level UP");
            //levelText.text = level.ToString(); // mostramos por pantalla la subida de nivel

        }

    }


    public void SaveGame() {
        // guardamos la informacion en el playerpref
        SaveSystem.instance.ExperienceSavingData(currentExp); // actualizamos el valor de la experiencia
        SaveSystem.instance.NextLevelExperienceSavingData(nextLevelExp); // actualizamos el valor de la experiencia necesaria para subir de nivel
        SaveSystem.instance.LevelSavingData(level); // actualizamos el valor del nivel
        SaveSystem.instance.HealthSavingData(PlayerHealth.instance.maxHealth);
        SaveSystem.instance.MagicSavingData(PlayerHealth.instance.maxMana);
        SaveSystem.instance.STRSavingData(PlayerControler.instance.strenght);
        SaveSystem.instance.DEFSavingData(PlayerControler.instance.defense);
        SaveSystem.instance.ATTCKSavingData(PlayerControler.instance.attackWeapon);
        SaveSystem.instance.GOLDSavingData(totalMoney.instance.moneyInTotal);

        //parametros a guardar
        currentExp = PlayerPrefs.GetFloat("currentExp");
        nextLevelExp = PlayerPrefs.GetFloat("nextLevelEXP");
        level = PlayerPrefs.GetInt("level");
        PlayerHealth.instance.maxHealth = PlayerPrefs.GetFloat("maxHealth");
        PlayerHealth.instance.maxMana = PlayerPrefs.GetFloat("maxMana");
        PlayerControler.instance.strenght = PlayerPrefs.GetFloat("STR");
        PlayerControler.instance.defense = PlayerPrefs.GetFloat("DEF");
        PlayerControler.instance.attackWeapon = PlayerPrefs.GetFloat("ATTCK");
        totalMoney.instance.moneyInTotal = PlayerPrefs.GetFloat("Gold");
    }

    IEnumerator textAbove(GameObject text)
    {
        Vector2 initPos = new Vector2(text.transform.position.x, text.transform.position.y); // marcamos la posicion inicial de donde sale el texto
        Vector2 finalPos = new Vector2(text.transform.position.x, text.transform.position.y + 40); // dejamos el eje x como estaba y aumentamos el eje y 20 unidades
        int upTimes = 0; // veces que ejecutara el while
        while (upTimes < 6)
        { //mientras el upTimes sea menor de 6 se correra el bucle
            upTimes++; // incrementamos el upTimes
            text.transform.position = Vector2.MoveTowards(initPos, finalPos, 15f * Time.deltaTime); // el gameobject del texto se movera hasta la posicion final
            yield return new WaitForEndOfFrame(); // cada vez que pase un frame se vuelve a correr el while
        }


    }
}
