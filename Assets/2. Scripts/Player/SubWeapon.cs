using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubWeapon : MonoBehaviour
{
    public int neededHearts; // variable que indica el numero de corazones que cuesta usar la arma secundaria
    public GameObject axe; // variable que determina al objeto sub weapon, en este caso un hacha


    // Update is called once per frame
    void Update()
    {
        UseSubWeapon();
    }

    public void UseSubWeapon() {

        // para diferencia el uso de las armas secundarias de las magias y poder usar el mismo boton de accion
        // haremos que el arma secundaria solo sea disbarada al pulsar direccion hacia arriba + Fire2
        if (Input.GetAxisRaw("Vertical") > 0) {

            if (Input.GetButtonDown("Fire2") && neededHearts <= Hearts.instance.heartsInTotal) { // si el usuario presiona el boton derecho del raton o alt y tiene los corazones necesarios (el coste sea inferior al total que tiene en ese momento el jugador) podra disparar el arma secundaria
                Hearts.instance.subItem(-neededHearts); // restaremos los corazones a la instancia de la clase hearts que determina el total de corazones en ese momento

                GameObject subItem = Instantiate(axe, transform.position, Quaternion.Euler(0, 0, 0));


                if (transform.localScale.x < 0)
                { // si el jugador esta mirando a la izquierda el hacha ira a la izquierda
                    subItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(-500f, 0f), ForceMode2D.Force);
                    subItem.transform.localScale = new Vector2(-1, -1); // giramos el hacha para que vaya en la correcta direccion
                }
                else
                { // si mira a la derecha el hacha ira en esa direccion
                    subItem.GetComponent<Rigidbody2D>().AddForce(new Vector2(500f, 0f), ForceMode2D.Force);
                }
            }
        }
 
    }
}
